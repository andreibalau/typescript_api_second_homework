# Javascript Course 23 July

##Notes
This is what we've done today.

I made the following changes:

1. Deleted /resources :)
2. Made all the DB calls using async/await
3. Surrounded every DriverManager call with try-catch
4. To proper use the async/await syntax, I had to use .exec() in DriverManager on queries.

Run `npm install` within this folder after cloning to install the package needed to run.
