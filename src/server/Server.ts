import express from "express";
import bodyParser from "body-parser";
import DriverManager from "../services/DriverManager";

export default class Server {
	private port: number;
	private app: express.Application;

	public constructor(app: express.Application, port: number) {
		this.port = port;
		this.app = app;

		this.configApp();
		this.setRoutes();

		DriverManager.Instance.connect();

		this.startServer();
	}

	private startServer() {
		this.app.listen(this.port, () => {
			console.log(`Server started at http://localhost:${this.port}!`);
		});
	}

	private configApp() {
		// parse application/x-www-form-urlencoded from body
		this.app.use(bodyParser.json());

		//parse application.json from body
		this.app.use(bodyParser.urlencoded({ extended: false }));
	}

	private setRoutes() {
		this.app.get(
			"/",
			(request: express.Request, response: express.Response) => {
				response.send("Hello world");
			}
		);

		this.app.get(
			"/lists",
			async (req: express.Request, res: express.Response) => {
				try {
					let lists = await DriverManager.Instance.getAllToDoLists();
					if (lists.length > 0) {
						res.send(lists);
					} else {
						res.send("No list found!");
					}
				} catch {
					res.send("Error!");
				}
			}
		);

		this.app.get(
			"/list/:name",
			async (req: express.Request, res: express.Response) => {
				let nameFromRequest = req.params.name;

				try {
					let requestList = await DriverManager.Instance.getListByName(
						nameFromRequest
					);
					res.send(requestList);
				} catch {
					res.send("Error!");
				}
			}
		);

		this.app.post(
			"/addList",
			async (req: express.Request, res: express.Response) => {
				try {
					await DriverManager.Instance.addNewList(req.body.name);
					res.send("Success!");
				} catch {
					res.send("Error!");
				}
			}
		);

		this.app.post(
			"/addListElement",
			async (req: express.Request, res: express.Response) => {
				try {
					await DriverManager.Instance.addNewElementForList(
						req.body.listName,
						req.body.taskName,
						req.body.description
					);

					res.send("Success!");
				} catch {
					res.send("Error!");
				}
			}
		);

		this.app.delete(
			"/deleteList",
			async (req: express.Request, res: express.Response) => {
				try {
					await DriverManager.Instance.deleteList(
						req.body.listName
					);
					res.send("Success!");
				} catch {
					res.send("Error!");
				}
			}
		);

		this.app.delete(
			"/deleteListElement",
			async (req: express.Request, res: express.Response) => {
				try {
					await DriverManager.Instance.deleteListElement(
						req.body.listName,
						req.body.taskName
					);
					res.send("Success!");
				} catch {
					res.send("Error!");
				}
			}
		);
	}
}