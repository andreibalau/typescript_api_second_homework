import { IListElement, ListElementSchema } from "./ListElement";
import mongoose, { Schema, Document } from "mongoose";

export interface IToDoList extends Document {
	name: string;
	listElements: [IListElement];
}

const ToDoListSchema = new Schema(
	{
		name: {
			type: String,
			required: true,
			unique: true,
			index: true
		},
		listElements: {
			type: [ListElementSchema]
		}
	},
	{ collection: "ToDoList" }
);

export default mongoose.model<IToDoList>("ToDoList", ToDoListSchema);